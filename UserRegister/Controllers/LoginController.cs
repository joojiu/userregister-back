﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserRegister.DAO;
using UserRegister.Models;

namespace UserRegister.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private LoginDAO loginDAO = new LoginDAO();

        [HttpPost]
        public bool Login(Login login)
        {
            var logInto = this.loginDAO.Login(login);
            if(logInto.UserName == null || logInto.Password == null)
            {
                return false;
            }
            return true;
        }

        [HttpPost]
        public void RegisterLogin(RegisterUser user)
        {
            this.loginDAO.RegisterLogin(user);
        }
    }
}