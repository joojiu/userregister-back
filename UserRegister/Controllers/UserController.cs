﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using UserRegister.DAO;
using UserRegister.Models;

namespace UserRegister.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        UserDAO userDAO = new UserDAO();

        // POST: api/User
        [HttpPost]
        public void InsertUser(User user)
        {
            userDAO.InsertUser(user);
        }

        [HttpPut("{id}")]
        public void UpdateUser(User user)
        {
            userDAO.UpdateUser(user);
        }

        [HttpDelete("{id}")]
        public void DeleteUser(int id)
        {
            userDAO.DeleteUser(id);
        }

        [HttpGet]
        public List<User> GetAll()
        {
            return userDAO.GetAll();
        }

        [HttpGet("{id}")]
        public User GetUserById(int id)
        {
            return userDAO.GetUserById(id);
        }
    }
}