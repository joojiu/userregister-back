﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using UserRegister.Models;

namespace UserRegister.DAO
{
    public class UserDAO
    {
        private SqlConnection connection = new SqlConnection(@"Persist Security Info=False;User ID=jooji;Password=123456;Initial Catalog=jooji;Data Source=PLTW10N07412\SQLEXPRESS");

        public void InsertUser(User user)
        {
            string commandText = "INSERT INTO Users(name, code, typeUser, age) VALUES (@name, @code, @typeUser, @age)";
            SqlCommand cmd = new SqlCommand(commandText, connection);
            cmd.Parameters.AddWithValue("@name", user.Name);
            cmd.Parameters.AddWithValue("@code", user.Code);
            cmd.Parameters.AddWithValue("@typeUser", user.Type);
            cmd.Parameters.AddWithValue("@age", user.Age);

            try
            {
                connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public void UpdateUser(User user)
        {
            string commandText = "UPDATE Users SET name = @name, code = @code, typeUser = @type, age = @age WHERE id = @id";
            SqlCommand cmd = new SqlCommand(commandText, connection);

            cmd.Parameters.AddWithValue("@id", user.Id);
            cmd.Parameters.AddWithValue("@name", user.Name);
            cmd.Parameters.AddWithValue("@code", user.Code);
            cmd.Parameters.AddWithValue("@type", user.Type);
            cmd.Parameters.AddWithValue("@age", user.Age);

            try
            {
                connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public void DeleteUser(int id)
        {
            string commandText = "DELETE FROM Users WHERE id = " + id;
            SqlCommand cmd = new SqlCommand(commandText, connection);

            try
            {
                connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public List<User> GetAll()
        {
            string commandText = "SELECT * FROM Users";
            SqlCommand cmd = new SqlCommand(commandText, connection);

            try
            {
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                List<User> userList = new List<User>();

                while (reader.Read())
                {
                    User user = new User();
                    user.Id = Convert.ToInt32(reader["id"]);
                    user.Name = Convert.ToString(reader["name"]);
                    user.Code = Convert.ToInt32(reader["code"]);
                    user.Type = Convert.ToString(reader["typeUser"]);
                    user.Age = Convert.ToInt32(reader["age"]);

                    userList.Add(user);
                }
                return userList;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public User GetUserById(int id)
        {
            string commandText = "SELECT * FROM Users WHERE id = @id";
            SqlCommand cmd = new SqlCommand(commandText, connection);

            cmd.Parameters.AddWithValue("@id", id);

            try
            {
                connection.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                User user = new User();

                if (reader.Read())
                {
                    user.Id = Convert.ToInt32(reader["id"]);
                    user.Name = Convert.ToString(reader["name"]);
                    user.Code = Convert.ToInt32(reader["code"]);
                    user.Type = Convert.ToString(reader["typeUser"]);
                    user.Age = Convert.ToInt32(reader["age"]);
                }
                return user;
                
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
