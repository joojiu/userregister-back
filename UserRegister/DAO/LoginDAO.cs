﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using UserRegister.Models;

namespace UserRegister.DAO
{
    public class LoginDAO
    {
        private SqlConnection connection = new SqlConnection(@"Persist Security Info=False;User ID=jooji;Password=123456;Initial Catalog=jooji;Data Source=PLTW10N07412\SQLEXPRESS");
        public Login Login(Login login)
        {
            string commandText = "SELECT loginName, password FROM Employee WHERE loginName = @username AND password = @password";
            SqlCommand cmd = new SqlCommand(commandText, connection);

            cmd.Parameters.AddWithValue("@username", login.UserName);
            cmd.Parameters.AddWithValue("@password", login.Password);

            try
            {
                connection.Open();
                var reader = cmd.ExecuteReader();
                Login loginAll = new Login();

                while (reader.Read())
                {
                    loginAll.UserName = Convert.ToString(reader["login"]);
                    loginAll.Password = Convert.ToString(reader["password"]);
                }

                return loginAll;
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public void RegisterLogin(RegisterUser user)
        {
            string commandText = "INSERT INTO RegisterUser (name, lastName, email, password) VALUES (@name, @lastname, @email, @password)";
            SqlCommand cmd = new SqlCommand(commandText, connection);

            cmd.Parameters.AddWithValue("@name", user.Name);
            cmd.Parameters.AddWithValue("@lastname", user.LastName);
            cmd.Parameters.AddWithValue("@email", user.Email);
            cmd.Parameters.AddWithValue("@password", user.Password);

            try
            {
                connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch(Exception ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
    }
}
